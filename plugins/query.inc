<?php

/**
 * @file
 *
 */

class luceneapi_views_plugin_query extends views_plugin_query {

  /**
   *
   */
  protected $_searcher;

  /**
   *
   */
  protected $_fields = array();

  /**
   *
   */
  protected $_filters = array();

  /**
   *
   */
  protected $_sorts = array();

  /**
   *
   */
  function init($base_table, $base_field, $options) {
    parent::init($base_table, $base_field, $options);
    $this->_searcher = new LuceneapiViewsSearcher($base_table);
  }

  /**
   *
   */
  function get_searcher() {
    return $this->_searcher;
  }

  /**
   *
   */
  function add_field($field) {
    $this->_fields[] = $field;
  }

  /**
   *
   */
  function add_filter($field, $query) {
    $this->_filters[$field] = $query;
  }

  /**
   *
   */
  function add_sort($field, $order) {
    $this->_sorts[$field] = $order;
  }

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->init_pager();
    $this->pager->query();

    $view->build_info['query'] = 'Query placeholder';
  }

  /**
   * Executes the query and fills the associated view object with according
   * values.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   */
  function execute(&$view) {
    $index = $this->_searcher->getIndex();

    // Builds the query object.
    $query = array(
      '#limit' => $this->limit,
      '#sort' => $this->_sorts,
    );
    foreach ($this->_filters as $field => $filter) {
      $query[$field] = $filter;
    }

    // Sets the total items.
    $this->pager->total_items = $view->total_rows = $this->_searcher->getTotalRows();

    // Starts the timer.
    $start = microtime(TRUE);

    // Executes the search query.
    $view->result = array();
    foreach ($this->_searcher->search($query) as $hit) {
      $view->result[] = $hit;
    }

    // Finalizes the time it took to execute the query.
    $view->execute_time = microtime(TRUE) - $start;
  }

  /**
   * Get aggregation info for group by queries.
   *
   * If NULL, aggregation is not allowed.
   */
  function get_aggregation_info() { }

}
