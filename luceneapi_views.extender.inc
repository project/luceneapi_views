<?php

/**
 * @file
 */

/**
 *
 */
class LuceneapiViewsSearcher extends LuceneapiSearcher {

  /**
   * Skips CTools stuff because Views handles the search page.
   */
  public function __construct($name) {

    // Fakes the settings object.
    $this->_settings = new stdClass();
    $this->_settings->name = $name;
    $this->_settings->description = '';
    $this->_settings->index_name = substr($name, strpos($name, ':') + 1);
    $this->_settings->search_page = '';
    $this->_settings->settings = array();

    // Loads the index.
    $this->_loadIndex();
  }
}
