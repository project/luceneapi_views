<?php

/**
 * @file
 * Views hook implementations.
 */

/**
 * Implements hook_views_plugins().
 */
function luceneapi_views_views_plugins() {
  return array(
    'query' => array(
      'luceneapi_views_query' => array(
        'title' => t('Search Lucene API Query'),
        'help' => t('Queries indexes managed by Search Lucene API.'),
        'handler' => 'luceneapi_views_plugin_query'
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function luceneapi_views_views_data() {
  $data = array();

  try {
    // Gets index list directly from the database.
    $result = db_select('luceneapi_index', 'i')
      ->fields('i', array('name', 'label', 'description'))
      ->execute();
  }
  catch (Exception $e) {
    watchdog_exception('luceneapi', $e);
    $result = array();
  }

  // Gets field type to handler mappings.
  $mappings = luceneapi_views_get_handler_mappings();
  $stored = array('keyword', 'stored', 'text');
  $indexed = array('keyword', 'unstored', 'text');

  // Iterates over indexes, adds one base item per index.
  foreach ($result as $record) {
    // Attempts to load the index, moves onto next one on errors.
    if (!$index = luceneapi_index_load($record->name)) {
      continue;
    }

    // Builds base table name.
    $table = 'luceneapi:' . $record->name;

    $data[$table]['table']['group'] = $record->label;

    $data[$table]['table']['base'] = array(
      'title' => t('Search Lucene API: @label', array('@label' => $record->label)),
      'help' => t('Queries the %label index managed by Search Lucene API.', array('%label' => $record->label)),
      'query class' => 'luceneapi_views_query',
    );

    // @todo Move this somewhere else maybe?  Let's not assume node.
    if ('luceneapi_node' == $index->getSettings()->indexer) {
      $data[$table]['table']['base']['field'] = 'nid';
    }

    $data[$table]['keys'] = array(
      'title' => t('Search keys'),
      'help' => t('The terms to search for.'),
      'filter' => array(
        'handler' => 'luceneapi_views_handler_filter_keys',
      ),
    );

    // Iterates over enabled fields.
    foreach ($index->getEnabledFields() as $field_name => $field) {
      if (!isset($mappings[$field['normalize hook']])) {
        continue;
      }

      // Initializes table.
      $data[$table][$field_name] = array(
        'title' => $field['label'],
        'help' => $field['description'],
      );

      // Adds field mapping if the source is stored in the index.
      if (in_array($field['field type'], $stored)) {
        $data[$table][$field_name]['field'] = $mappings[$field['normalize hook']]['field'];
      }

      // Adds filter mapping if indexed.
      if (in_array($field['field type'], $indexed)) {
        $data[$table][$field_name]['filter'] = $mappings[$field['normalize hook']]['filter'];
      }

      // Adds sort if field is sortable.
      if ($field['sortable']) {
        $data[$table][$field_name]['sort'] = array(
          'handler' => 'luceneapi_views_handler_sort',
        );
      }

    }

  }

  return $data;
}

