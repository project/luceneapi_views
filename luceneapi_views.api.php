<?php

/**
 * @file
 *
 */

/**
 * Maps normalize hooks to handlers.
 */
function hook_luceneapi_views_handler_mappings() {

}

/**
 * Alters mappings.
 */
function hook_luceneapi_views_handler_mappings_alter(&$mappings) {

}
