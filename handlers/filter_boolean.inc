<?php

/**
 *
 */
class luceneapi_views_handler_filter_boolean extends views_handler_filter_boolean_operator {

  function query() {
    // @todo Eliminate "use equal", "accept_null" setting.
    return $this->query->add_filter($this->real_field, array(
      '#type' => 'term',
      '#text' => $this->value,
      '#field' => $this->real_field,
      '#sign' => TRUE,
    ));
  }
}