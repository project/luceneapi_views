<?php

/**
 *
 */
class luceneapi_views_handler_field_boolean extends views_handler_field_boolean {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->query->add_field($this->real_field);
  }

  /**
   *
   */
  function render($values) {
    $this->field_alias = $this->real_field;

    // @todo We need a "denormalize" hook or something.
    $value = $values->{$this->real_field};
    if (is_numeric($value)) {
      $values->{$this->real_field} = (int) $value;
    }
    elseif (is_string($value)) {
      $values->{$this->real_field} = ('F' != $value);
    }
    $values->{$this->real_field} = (bool) $value;

    return parent::render($values);
  }
}
