<?php

/**
 * @file
 */

/**
 *
 */
class luceneapi_views_handler_sort extends views_handler_sort {

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $order = ('DESC' != $this->options['order']) ? SORT_ASC : SORT_DESC;
    $this->query->add_sort($this->real_field, $order);
  }
}