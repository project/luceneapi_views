<?php

/**
 * Simple filter to handle greater than/less than filters
 */
class luceneapi_views_handler_filter_numeric extends views_handler_filter_numeric {

  /**
   *
   */
  function operators() {
    $operators = array(
      '<' => array(
        'title' => t('Is less than'),
        'method' => 'op_between',
        'short' => t('<'),
        'values' => 1,
      ),
      '<=' => array(
        'title' => t('Is less than or equal to'),
        'method' => 'op_between',
        'short' => t('<='),
        'values' => 1,
      ),
      '=' => array(
        'title' => t('Is equal to'),
        'method' => 'op_simple',
        'short' => t('='),
        'values' => 1,
      ),
      '!=' => array(
        'title' => t('Is not equal to'),
        'method' => 'op_simple',
        'short' => t('!='),
        'values' => 1,
      ),
      '>=' => array(
        'title' => t('Is greater than or equal to'),
        'method' => 'op_between',
        'short' => t('>='),
        'values' => 1,
      ),
      'between' => array(
        'title' => t('Is between'),
        'method' => 'op_between',
        'short' => t('between'),
        'values' => 2,
      ),
      'not between' => array(
        'title' => t('Is not between'),
        'method' => 'op_between',
        'short' => t('not between'),
        'values' => 2,
      ),
    );

    return $operators;
  }

  function query() {
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($this->real_field);
    }
  }

  /**
   *
   */
  function op_simple($field) {
    $sign = ('=' == $this->operator) ? TRUE : FALSE;
    return $this->query->add_filter($field, array(
      '#type' => 'term',
      '#text' => $this->value['value'],
      '#field' => $field,
      '#sign' => $sign,
    ));
  }

  /**
   *
   */
  function op_between($field) {
    switch ($this->operator) {
      case '<':
        return $this->query->add_filter($field, array(
          '#type' => 'range',
          '#lower_term' => 0,
          '#upper_term' => $this->value['max'],
          '#is_inclusive' => FALSE,
          '#field' => $field,
          '#sign' => NULL,
        ));

      case '<=':
        return $this->query->add_filter($field, array(
          '#type' => 'range',
          '#lower_term' => 0,
          '#upper_term' => $this->value['max'],
          '#is_inclusive' => TRUE,
          '#field' => $field,
          '#sign' => NULL,
        ));

      case '=':
        return $this->query->add_filter($field, array(
          '#type' => 'term',
          '#text' => $this->value['value'],
          '#field' => $field,
          '#sign' => TRUE,
        ));

      case '!=':
        return $this->query->add_filter($field, array(
          '#type' => 'term',
          '#text' => $this->value['value'],
          '#field' => $field,
          '#sign' => FALSE,
        ));

      case '>=':
        return $this->query->add_filter($field, array(
          '#type' => 'range',
          '#lower_term' => $this->value['min'],
          '#upper_term' => 9999999999,
          '#is_inclusive' => TRUE,
          '#field' => $field,
          '#sign' => NULL,
        ));

      case 'between':
        return $this->query->add_filter($field, array(
          '#type' => 'range',
          '#lower_term' => $this->value['min'],
          '#upper_term' => $this->value['max'],
          '#is_inclusive' => TRUE,
          '#field' => $field,
          '#sign' => NULL,
        ));

      case 'not between':
        return $this->query->add_filter($field, array(
          '#type' => 'range',
          '#lower_term' => $this->value['min'],
          '#upper_term' => $this->value['max'],
          '#is_inclusive' => TRUE,
          '#field' => $field,
          '#sign' => FALSE,
        ));
    }
  }
}
