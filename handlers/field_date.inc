<?php

/**
 * @file
 *
 */

/**
 *
 *
 */
class luceneapi_views_handler_field_date extends views_handler_field_date {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->query->add_field($this->real_field);
  }

  /**
   *
   */
  function render($values) {
    $this->field_alias = $this->real_field;
    if (!is_numeric($values->{$this->real_field})) {
      $values->{$this->real_field} = strtotime($values->{$this->real_field});
    }
    return parent::render($values);
  }
}