<?php

/**
 * @file
 *
 */

/**
 *
 *
 */
class luceneapi_views_handler_field extends views_handler_field {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->query->add_field($this->real_field);
  }

  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  function render($values) {
    $this->field_alias = $this->real_field;
    return parent::render($values);
  }

}