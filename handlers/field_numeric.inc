<?php

/**
 *
 */
class luceneapi_views_handler_field_numeric extends views_handler_field_numeric {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->query->add_field($this->real_field);
  }

  /**
   *
   */
  function render($values) {
    $this->field_alias = $this->real_field;
    return parent::render($values);
  }
}
