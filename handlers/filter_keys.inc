<?php

/**
 * @file
 */

/**
 *
 */
class luceneapi_views_handler_filter_keys extends views_handler_filter {

  /**
   * Provide a simple textfield for equality.
   */
  function exposed_form(&$form, &$form_state) {
    if (isset($this->options['expose']['identifier'])) {
      $key = $this->options['expose']['identifier'];
      $form[$key] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->value,
        '#attributes' => array('title' => t('Enter the terms you wish to search for.')),
      );
    }
  }

  /**
   * Add this filter to the query.
   *
   * Due to the nature of fapi, the value and the operator have an unintended
   * level of indirection. You will find them in $this->operator
   * and $this->value respectively.
   */
  function query() {

    // @todo Figure out grouping.
    // dsm($this->options['group']);

    $query = array();
    foreach ($this->value as $value) {
      $this->query->add_filter($this->real_field, array(
        '#type' => 'keys',
        '#text' => $value,
      ));
    }
  }

}
